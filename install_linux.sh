#!/usr/bin/env bash
# change username and password
#export USERNAME=YOUR_USERNAME
#export PASSWORD=YOUR_PASSWORD
# goto pipeline parent folder and call tris script
#wget https://$USERNAME:$USERNAME@bitbucket.org/pw_studio_dev/studio_pipeline/raw/b26b48bda60a17366bfdd70f67f44ad964212085/install_tools.sh
#chmod +x ./install_tools.sh
#linux/install_pipeline_scripts.sh
cecho() {
    CST='\033[1;33m'
    CEN='\033[0m'
    echo -e "${CST}$*${CEN}"
}

START_DIR=$PWD
PIPELINE_FOLDER_NAME=studio_pipeline
PIPELINE_LOCATION=${START_DIR}/${PIPELINE_FOLDER_NAME}
SHORTCUT_NAME="pipeline_launcher"
sudo ls >> /dev/null
cecho "Start installation..."
echo -n "Enter BitBucket Login: "
read USERNAME
echo -n "Enter BitBucket password: "
read -s PASSWORD
echo
if [ -z "${USERNAME}" ]; then
    echo "USERNAME is unset" && exit 1
fi
if [ -z "${PASSWORD}" ]; then
    echo "PASSWORD is unset" && exit 1
fi

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' git|grep "install ok installed")
if [ "" == "$PKG_OK" ]; then
    cecho "-----------------------------------"
    cecho "INSTALL GIT"
    cecho "-----------------------------------"
    sudo apt-get install -y git
fi


cecho "-----------------------------------"
cecho "INSTALL PIPELINE"
cecho "-----------------------------------"
cecho "Install core..."
git clone https://$USERNAME:$PASSWORD@bitbucket.org/pw_studio_dev/studio_pipeline.git
# Create Root config from example
cd ${PIPELINE_LOCATION}
#mv config_example.json config.json
echo {} >>config.json

cecho "Install tools..."
cd ${PIPELINE_LOCATION}/tools
git clone https://$USERNAME:$PASSWORD@bitbucket.org/pw_studio_dev/pipeline_starter.git
git clone https://$USERNAME:$PASSWORD@bitbucket.org/fenx/fenx.config.git pipeline_config
git clone https://$USERNAME:$PASSWORD@bitbucket.org/pw_studio_dev/pipeline_menu_generator.git

# python install
cecho "-----------------------------------"
cecho "INSTALL PYTHON ENV"
cecho "-----------------------------------"
# python tools
sudo apt-get -y install python-dev python-pip libpq-dev python-pyside
cecho "Upgrade pip"
sudo pip install --upgrade pip
sudo pip install -U setuptools

VENV_OK=$(which virtualenv)
if [ "" == "$VENV_OK" ]; then
    echo "Install VIRTUALENV"
    sudo pip install virtualenv
fi
#virtualenvwrapper

file="/usr/local/bin/virtualenvwrapper.sh"
if [ -f "$file" ]
then
	cecho "$file found."
else
	sudo pip install virtualenvwrapper
fi

if  [ -z  ${VIRTUALENVWRAPPER_PYTHON} ];then
   echo "export VIRTUALENVWRAPPER_PYTHON='/usr/bin/python'" >> ~/.bashrc
   echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
else
   echo "VIRTUALENVWRAPPER_PYTHON var exists"
fi

export VIRTUALENVWRAPPER_PYTHON='/usr/bin/python'
source /usr/local/bin/virtualenvwrapper.sh
cecho "Create virtualenv"

ENV_EXISTS=${WORKON_HOME}/studio_env
if [ -d "$ENV_EXISTS" ]
then
	cecho "$ENV_EXISTS already exists."
else
    echo "Create new virtual env named 'studio_env'..."
    echo
	PYVER="$(ls /usr/lib | grep python2 | head -n 1)"
    mkvirtualenv -r ${PIPELINE_LOCATION}/python/requirements_linux.txt --python=/usr/bin/${PYVER} --system-site-packages studio_env

fi


chmod +x ${PIPELINE_LOCATION}/start_tray.sh
chmod +x ${PIPELINE_LOCATION}/start_app.sh

# shortcuts
cecho "-----------------------------------"
cecho "CREATE SHORTCUTS"
cecho "-----------------------------------"

cecho "Crate directories"
echo ~/.local/share/applications
mkdir -p ~/.local/share/applications
echo ~/.config/autostart
mkdir -p ~/.config/autostart

cecho "Create menu shortcut"
echo ~/.local/share/applications/${SHORTCUT_NAME}.desktop
cat > ~/.local/share/applications/${SHORTCUT_NAME}.desktop <<EOL
[Desktop Entry]
Version=1.0
Type=Application
Name=Pipeline Launcher
Comment=Studio pipeline launcher
Exec=${PIPELINE_LOCATION}/start_tray.sh
Icon=${PIPELINE_LOCATION}/tools/pipeline_starter/icons/menu.png
Path=${PIPELINE_LOCATION}
Terminal=false
StartupNotify=false
Keywords=Pipeline;cg;starter;launcher
Categories=GNOME;GTK;Graphics;3DGraphics;
X-GNOME-Autostart-Delay=5
EOL
sudo chmod +x ~/.local/share/applications/${SHORTCUT_NAME}.desktop

cecho "Create menu shortcut with terminal"
echo ~/.local/share/applications/${SHORTCUT_NAME}_t.desktop
cat > ~/.local/share/applications/${SHORTCUT_NAME}_t.desktop <<EOL
[Desktop Entry]
Version=1.0
Type=Application
Name=Pipeline Launcher (terminal)
Comment=Studio pipeline launcher with terminal
Exec=${PIPELINE_LOCATION}/start_tray.sh
Icon=${PIPELINE_LOCATION}/tools/pipeline_starter/icons/menu.png
Path=${PIPELINE_LOCATION}
Terminal=true
StartupNotify=false
Keywords=Pipeline;cg;starter;launcher
Categories=GNOME;GTK;Graphics;3DGraphics;
X-GNOME-Autostart-Delay=5
EOL
sudo chmod +x ~/.local/share/applications/${SHORTCUT_NAME}_t.desktop

cecho "Create startup shortcut"
echo ~/.config/autostart/${SHORTCUT_NAME}.desktop
cp ~/.local/share/applications/${SHORTCUT_NAME}.desktop ~/.config/autostart

echo && echo
cecho "==========================================================================================="
cecho "   INSTALLATION COMPLETE!"
cecho "==========================================================================================="
cecho "   WHAT NEXT ?"
cecho "1. Edit config.json for you studio pipeline"
cecho "2. Mount this folder on workstations (read only)"
cecho "3. Add start_tray.sh to startup apps on workstations "
echo "     example: /home/${USER}/.config/autostart/${SHORTCUT_NAME}.desktop"
cecho "4. Reboot system..."
echo
echo


