@ECHO off

set INSTALL_DIR=%~dp0
echo UPDATE STUDIO PIPELINE
git pull
echo UPDATE TOOLS
for /d %%d in (%INSTALL_DIR%tools\*) do (
    cd %%d
    git pull
)

set UNINST_WIN_URL=https://bitbucket.org/pw_studio_dev/studio_pipeline_install_scripts/raw/master/uninstall_windows.cmd
CALL powershell -Command "(New-Object Net.WebClient).DownloadFile('%UNINST_WIN_URL%','%INSTALL_DIR%\uninstall.cmd')"
set UNINST_WIN_URL=https://bitbucket.org/pw_studio_dev/studio_pipeline_install_scripts/raw/master/update_windows.cmd
CALL powershell -Command "(New-Object Net.WebClient).DownloadFile('%UNINST_WIN_URL%','%INSTALL_DIR%\update.cmd')"

echo UPDATE COMPLETE

pause
