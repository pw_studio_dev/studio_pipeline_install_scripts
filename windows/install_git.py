import requests, sys, re, os, tempfile


def download_file(url, path):
    local_filename = os.path.normpath(os.path.join(path, os.path.basename(url)))
    if os.path.exists(local_filename):
        os.remove(local_filename)
    resp = requests.get(url, stream=True)
    total_length = int(resp.headers.get('content-length'))
    block_size = 1024 * 4
    dl = 0
    with open(local_filename, 'wb') as f:
        for chunk in resp.iter_content(chunk_size=block_size):
            if chunk:
                f.write(chunk)
                f.flush()
                dl += len(chunk)
                done = int(50 * dl / total_length)
                sys.stdout.write("\r[%s%s] %sMb of %sMb" % ('=' * done,
                                                            ' ' * (50 - done),
                                                            int(dl / 1024.0 / 1024.0),
                                                            int(total_length / 1024.0 / 1024.0)
                                                            )
                                 )
                sys.stdout.flush()
    print
    print 'Download complete'
    return local_filename


# check is exists
git_executable = r'C:\Program Files\Git\bin\git.exe'
if os.path.exists(git_executable):
    sys.exit()

TEMP_DIR = os.getenv('TEMP_DIR') or tempfile.gettempdir()

# download git
git_url = 'https://git-scm.com/download/win'
page = requests.get(git_url, verify=False).content
s = re.search(r"<iframe .*?src='(.*?)'", page)
if not s:
    sys.exit(1)
git_exe = s.group(1)
path = download_file(git_exe, TEMP_DIR)
if not path:
    print 'Cant download Git Installation'
    sys.exit(1)

setup_file = r'''[Setup]
Lang=default
Dir=C:\Program Files\Git
Group=Git
NoIcons=0
SetupType=default
Components=ext,ext\reg,ext\reg\shellhere,assoc_sh
Tasks=
PathOption=Cmd
SSHOption=OpenSSH
CRLFOption=CRLFAlways'''
setup_file_path = os.path.join(TEMP_DIR, 'gitsetup.txt')
open(setup_file_path, 'w').write(setup_file)
commandLineOptions = ' /SP- /VERYSILENT /SUPPRESSMSGBOXES /FORCECLOSEAPPLICATIONS /LOADINF="{optionsFile}" /LOG="{logFile}"'.format(
    optionsFile=setup_file_path,
    logFile=os.path.join(TEMP_DIR, 'git_install.log')
)
# cmd = path + r' /SILENT /COMPONENTS="icons,ext\reg\shellhere,assoc,assoc_sh"'
cmd = path + commandLineOptions
print 'CALL:', cmd
os.system(cmd)
#
# if (!(Test-Path -Path "alias:git"))
# {
#    new-item -path alias:git -value 'C:\Program Files\Git\bin\git.exe'
# }
# ### Invoke git commands that set defaults for user.
# git config --global credential.helper wincred
# git config --global push.default simple
# git config --global core.autocrlf true
cms = r"""powershell.exe "new-item -path alias:git -value 'C:\Program Files\Git\bin\git.exe'" """
print 'CALL:', cmd
os.system(cmd)

# os.system('git config --global credential.helper wincred')
# os.system('git config --global push.default simple')
# os.system('git config --global core.autocrlf true')

print 'Git installed!'
print
