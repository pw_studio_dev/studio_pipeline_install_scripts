@ECHO off

:: VARIABLES
IF [%CD:~0,3%]==[%CD%] (
    echo   --^>   ERROR: Pipeline Scripts can not be installed to the root of drive
    EXIT /B
) ELSE (
    Echo
)
set START_DIR=%cd%
set INSTALL_DIR=%~dp0
:: Temp Folder
set TEMP_DIR=%START_DIR%\_temp
IF not exist %TEMP_DIR% (mkdir %TEMP_DIR%)
:: pipeline root
set PIPELINE_LOCATION=%START_DIR%\studio_pipeline
set PYTHON_LOCATION=%START_DIR%\python
set PYTHONEXE=%PYTHON_LOCATION%\python.exe

SET /p BBLOGIN="Enter BitBucket Login: "
SET /p BBPASSW="Enter BitBucket password (not secure!!!): "
cls

:: ====================================================================================================================
::       PYTHON
:: ====================================================================================================================

SET LOCALFILE=%TEMP_DIR%\python-2.7.6.amd64.msi
SET PIPFILE=%TEMP_DIR%\get-pip.py
SET PYTHONURL=https://www.python.org/ftp/python/2.7.14/python-2.7.14.amd64.msi
SET PIP_URL=https://raw.githubusercontent.com/pypa/get-pip/master/get-pip.py
echo ========= START INSTALL ===========
:: install python
IF EXIST %PYTHONEXE% (
echo   --^>   PYTHON ALREADY INSTALLED
) ELSE (
echo   --^>   DOWNLOAD PYTHON...
IF NOT EXIST %LOCALFILE% CALL powershell -Command "(New-Object Net.WebClient).DownloadFile('%PYTHONURL%','%LOCALFILE%')"
echo   --^>   INSTALL PYTHON...
start /wait msiexec /a "%LOCALFILE%" /qn TARGETDIR="%PYTHON_LOCATION%"
DEL /q %LOCALFILE%
)
set PYTHONEXE=%PYTHON_LOCATION%\python.exe
:: install  pip
IF EXIST %PYTHON_LOCATION%\Scripts\pip.exe (
echo   --^>   PIP ALREADY INSTALLED
) ELSE (
echo   --^>   INSTALL PIP...
powershell -Command "(New-Object Net.WebClient).DownloadFile('%PIP_URL%', '%PIPFILE%')"
set PYTHONHOME=
CALL %PYTHONEXE% %PIPFILE%
DEL /q %PIPFILE%
)
:: ====================================================================================================================
::      GIT
:: ====================================================================================================================
echo   --^>   INSTALL GIT...
SET GIT_INSTALL_SCRIPT_URL="https://bitbucket.org/pw_studio_dev/studio_pipeline_install_scripts/raw/master/windows/install_git.py"
SET GIT_INSTALL_SCRIPT_LOCALFILE=%TEMP_DIR%\install_git.py
:: start install
:: REM download install script
powershell -Command "(New-Object Net.WebClient).DownloadFile('%GIT_INSTALL_SCRIPT_URL%', '%GIT_INSTALL_SCRIPT_LOCALFILE%')"
%PYTHON_LOCATION%\Scripts\pip.exe install requests
call %PYTHONEXE% %GIT_INSTALL_SCRIPT_LOCALFILE%
SET GIT_BIN="C:\Program Files\Git\bin\git.exe"

:: DEL /q %GIT_INSTALL_SCRIPT_LOCALFILE%
:: timeout /t 2 /NOBREAK

:: ====================================================================================================================
::      PIPELINE
:: ====================================================================================================================
echo   --^>   INSTALL PIPELINE ...
cd /d %START_DIR%
:: Install pipeline starter
%GIT_BIN% clone https://%BBLOGIN%:%BBPASSW%@bitbucket.org/pw_studio_dev/studio_pipeline.git
cd /d %PIPELINE_LOCATION%
REM IF not exist "%TEMP_DIR%\install_tools.cmd" (echo "Error download" & exit /b)
:: Create Root config from example
echo {} >>config.json

echo   --^>   INSTALL REQUIREMENTS...
%PYTHON_LOCATION%\Scripts\pip.exe install -r %PIPELINE_LOCATION%\python\requirements_windows.txt

echo   --^>   INSTALL TOOLS...
SET TOOLS_DIR=%PIPELINE_LOCATION%\tools
IF not exist %TOOLS_DIR% (mkdir %TOOLS_DIR%)
cd /d %TOOLS_DIR%
echo   --^>   Install pipeline starter
%GIT_BIN% clone https://%BBLOGIN%:%BBPASSW%@bitbucket.org/pw_studio_dev/pipeline_starter.git
echo   --^>   Install config
%GIT_BIN% clone https://%BBLOGIN%:%BBPASSW%@bitbucket.org/fenx/fenx.config.git pipeline_config
echo   --^>   install menu generator
%GIT_BIN% clone https://%BBLOGIN%:%BBPASSW%@bitbucket.org/pw_studio_dev/pipeline_menu_generator.git

:: move python
DEL /q "%PIPELINE_LOCATION%\python"
RD /q "%PIPELINE_LOCATION%\python"
move %PYTHON_LOCATION% %PIPELINE_LOCATION%

echo   --^>   UPDATE AND UNINSTALL SHORTCUTS...
set UNINST_WIN_URL=https://bitbucket.org/pw_studio_dev/studio_pipeline_install_scripts/raw/master/uninstall_windows.cmd
CALL powershell -Command "(New-Object Net.WebClient).DownloadFile('%UNINST_WIN_URL%','%PIPELINE_LOCATION%\uninstall.cmd')"
set UNINST_WIN_URL=https://bitbucket.org/pw_studio_dev/studio_pipeline_install_scripts/raw/master/update_windows.cmd
CALL powershell -Command "(New-Object Net.WebClient).DownloadFile('%UNINST_WIN_URL%','%PIPELINE_LOCATION%\update.cmd')"

:: create startup link
echo   --^>   CREATE STARTUP SHORTCUT
echo   --^>   Shortcut in startup...

set SCRIPT="%TEMP%\script_%RANDOM%.vbs"
echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup\pipeline_tray.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.TargetPath = "%PIPELINE_LOCATION%\start_tray.cmd" >> %SCRIPT%
echo oLink.Save >> %SCRIPT%
cscript /nologo %SCRIPT%
del %SCRIPT%

:: create to programs link
echo   --^>   Shortcut in menu...

set SCRIPT="%TEMP%\script_%RANDOM%.vbs"
echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%APPDATA%\Microsoft\Windows\Start Menu\Programs\PipelineLauncher.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.TargetPath = "%PIPELINE_LOCATION%\start_tray.cmd" >> %SCRIPT%
echo oLink.Save >> %SCRIPT%
cscript /nologo %SCRIPT%
del %SCRIPT%

:: create to programs link with console
echo   --^>   Shortcut in menu with console...

set SCRIPT="%TEMP%\script_%RANDOM%.vbs"
echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%APPDATA%\Microsoft\Windows\Start Menu\Programs\PipelineLauncher (console).lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.TargetPath = "%PIPELINE_LOCATION%\start_tray.cmd" >> %SCRIPT%
echo oLink.Arguments = " -inl " >> %SCRIPT%
echo oLink.Save >> %SCRIPT%
cscript /nologo %SCRIPT%
del %SCRIPT%

echo   --^>   Cleanup

DEL /q "%TEMP_DIR%"
RD /q "%TEMP_DIR%"

echo
echo =========== INSTALL COMPLETE! =============

echo WHAT NEXT ????
echo 1. Edit config.json for you studio pipeline
echo 2. Map network drive with this folder in read only mode
echo 3. Add start_tray.cmd to startup folder on workstations
echo       "example: USERNAME\Start Menu\Programs\Startup
pause
