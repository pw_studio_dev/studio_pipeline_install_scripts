@ECHO off

set INSTALL_DIR=%~dp0

echo CHECK APP IS STARTED
echo ...skipped...

echo REMOVE SHORTCUTS...
del "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup\pipeline_tray.lnk"
del "%APPDATA%\Microsoft\Windows\Start Menu\Programs\PipelineLauncher.lnk"
del "%APPDATA%\Microsoft\Windows\Start Menu\Programs\PipelineLauncher (console).lnk"

echo REMOVE PIPELINE FOLDER
cd ..
:: todo: delete only package files
RD /S /Q %INSTALL_DIR%

:: Close app
:: remove pipeline folder except user files
::   del /S /Q %INSTALL_DIR%\*.dll
::   git rm -r -f -q *
::   git ls-files -z | xargs -0 rm -f
:: Remove virtual env
pause
