"""
This module check updates and update pipeline

run in ??? mode
create full backup of scripts, configs and all tools
update git repositories
restore from backup

mods:
    - check updates
    - create backup
    - restore from backup
    - update repositories
    - cleanup backups
    - create\delete scheduler task
"""
